/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 *
 * @author 34628
 */
public class Pruebas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String cadena_conexion = "jdbc:mysql://localhost:3307/templo";
        String usuario = "root";
        String contrasena = "1234";

        try {

            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();

            PreparedStatement stm = conn.prepareStatement("select * from Clase");
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                int id_templo = rs.getInt("id_templo");
                int id_profesor = rs.getInt("id_profesor");
                int id_alumno = rs.getInt("id_alumno");
                int id_tipo_clase = rs.getInt("id_tipo_clase");

                System.out.println("id " + id + " id_templo " + id_templo + " id_profesor " + id_profesor + " id_alumno " + id_alumno + " id_tipoclase " + id_tipo_clase);
            }

            rs.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(" ");
        try {

            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();

            PreparedStatement stm = conn.prepareStatement("select * from alumno");
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre_alumno = rs.getString("nombre_alumno");
                String rango = rs.getString("rango");
                Date fecha_inicio = rs.getDate("fecha_inicio");

                System.out.println(id + " " + nombre_alumno + " " + rango + " " + fecha_inicio);
            }

            rs.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("");
        try {

            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();

            PreparedStatement stm = conn.prepareStatement("select * from profesor");
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre_alumno = rs.getString("nombre_profesor");
                String rango = rs.getString("rango");
               
                System.out.println(id + " " + nombre_alumno + " " + rango);
            }

            rs.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(" ");
        try {

            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();

            PreparedStatement stm = conn.prepareStatement("select * from templo");
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre_templo = rs.getString("nombre_templo");
                String ubicacion = rs.getString("ubicacion");
               
                System.out.println(id + " " + nombre_templo + " " + ubicacion);
            }

            rs.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(" ");
         try {

            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();

            PreparedStatement stm = conn.prepareStatement("select * from tipo_clase");
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre_templo = rs.getString("nombre_clase");
                int ubicacion = rs.getInt("precio");
               
                System.out.println(id + " " + nombre_templo + " " + ubicacion);
            }

            rs.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
