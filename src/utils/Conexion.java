/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    private String cadena_conexion = "jdbc:mysql://localhost:3307/templo?user=root";
    private String usuario = "root";
    private String contra = "1234";

    public Conexion(String cadena_conexion, String usuario, String contra) {
        this.cadena_conexion = cadena_conexion;
        this.usuario = usuario;
        this.contra = contra;
    }

    public Connection getConnection() {
        try {
            Connection conn = DriverManager.getConnection(this.cadena_conexion, this.usuario, this.contra);
            return conn;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public void closeConnection(Connection conn) {
        try {
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
