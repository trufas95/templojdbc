/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import Controller.ClaseController;
import Controller.alumnoController;
import Controller.profesorController;
import Controller.temploController;
import Controller.tipo_claseController;
import Models.Clase;
import Models.Alumno;
import Models.Profesor;
import Models.Templo;
import Models.Tipo_clase;
import java.sql.Connection;
import java.util.Date;
import java.util.List;
import utils.Conexion;


public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//Crear una conexion y probarla.
        Conexion utilidades = new Conexion("jdbc:mysql://localhost:3307/templo?user=root", "root", "1234"); //Probar y sino quitar lo ultimo del path, poner hasta Templo.
        Connection conn = utilidades.getConnection();
//Probar cada metodo de los controladores.
        ClaseController rc = new ClaseController(conn);
        alumnoController ac = new alumnoController(conn);
        profesorController pc = new profesorController(conn);
        temploController tc = new temploController(conn);
        tipo_claseController tcc = new tipo_claseController(conn);

        //Listado de todas las List de cada clase.
        
        List<Clase> lista = rc.selectAllClase();
        for (Clase c : lista) {
            System.out.println(c);
        }
        
        System.out.println(" ");
        
        List <Alumno> lista1 = ac.selectAllalumno();
        for (Alumno a : lista1) {
            System.out.println(a);
        }
        
        System.out.println(" ");
        
        List <Profesor> lista2 = pc.selectAllprofesor();
        for (Profesor p : lista2) {
            System.out.println(p);
        }
        
        System.out.println(" ");
        
        List <Templo> lista3 = tc.selectAlltemplo();
        for (Templo t : lista3) {
            System.out.println(t);
        }
        
        System.out.println(" ");
        
         List <Tipo_clase> lista4 = tcc.selectAlltipo_clase();
        for (Tipo_clase w : lista4) {
            System.out.println(w);
        }
        
        //Insert, uno para cada clase
        System.out.println(" ");
        
        System.out.println("Insertamos un Profesor, id 57, nombre Sergio, rango Negro:");
       Profesor p = pc.insertprofesor(57, "Sergio", "Negro");
        System.out.println(p.toString());
        
        System.out.println("");
        
        System.out.println("Insertamos un templo con id 12, nombre rivasfu y localizacion en Rivas.");
        Templo t = tc.inserttemplo(12, "Rivasfu", "Rivas");
        System.out.println(t.toString());
        
        System.out.println("");
        
        System.out.println("Insertamos un tipo de clase con id 17, de nombre chikung y precio 35");
        Tipo_clase tipo = tcc.inserttipo_clase(17, "chikung", 35);
        System.out.println(tipo.toString());
        
        System.out.println(" ");
        
        System.out.println("Insertamos un nuevo alumno, con id 17, disciplina kungfu, Ramón Mascarpone, fecha inicio 03-05-2018");     
        Date date = new Date (03-05-2018);
        Alumno alumno = ac.insertalumno(17, "kungfu", "Ramon Mascarpone", date);
        System.out.println(alumno.toString());
        
        System.out.println(" ");
        
        System.out.println("Clase nueva con id varios 47 47 47 47 47");
        Clase clase = rc.insertClase(47, 47, 47, 47, 47);
        System.out.println(clase.toString());
        
        //prueba de deletes de cada clase
        
        System.out.println("");
        System.out.println("Borramos la clase de id 1");
        boolean clase1 = rc.deleteClase(1);
        System.out.println(clase1);
        
        System.out.println(" ");
        System.out.println("Borramos el alumno de id 1");
        boolean alumno1 = ac.deletealumno(1);
        System.out.println(alumno1);
        
        System.out.println(" ");
        System.out.println("Borramos el profesor de id 1");
        boolean profesor1 = pc.deleteprofesor(1);
        System.out.println(profesor1);
        
        System.out.println("");
        System.out.println("Borramos un templo de id 1");
        boolean templo1 = tc.deletetemplo(1);
        System.out.println(templo1);
        
        System.out.println("");
        System.out.println("Borramos un tipo_clase de id 1");
        boolean tipoClase = tcc.deletetipo_clase(1);
        System.out.println(tipoClase);
        System.out.println("");
        
        
        //UPDATE
        System.out.println("Actualizamos id_templo a 4 del templo que tenga id 1. ");
        Clase clase2 = rc.updateClase(4, 1);
        System.out.println(clase2);
        System.out.println("");
        
        System.out.println("Cambiamos el nombre del alumno de id 1 a Sonia.");
        Alumno alumno3 = ac.updatealumno("Sonia", 1);
        System.out.println(alumno3);
        System.out.println("");
        
        System.out.println("Cambiamos el nombre del profesor de id 1 a Juan");
        Profesor profesor3 = pc.updateprofesor("Juan", 1);
        System.out.println(profesor3);
        System.out.println("");
        
        System.out.println("Cambiamos el nombre del templo 1 a Puebla");
        Templo templo4 = tc.updatetemplo("Puebla", 1);
        System.out.println(templo4);
        System.out.println("");
        
        System.out.println("Cambiamos el nombre del tipo de clase del id 1 a Borracho");
        Tipo_clase tip1 = tcc.updatetipo_clase("Borracho", 1);
        System.out.println(tip1);
        
        
        utilidades.closeConnection(conn);

    }

}
