/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

public interface profesorDOD {

    public List<Profesor> selectAllprofesor();
    public Profesor insertprofesor(int id, String nombre_profesor, String rango);
    public boolean deleteprofesor(int id);
    public Profesor updateprofesor(String nombre_profesor, int id);
    //extra
    public ResultSet selectprofesor(String nombre_profesor);
}
