/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author 34628
 */
public class Clase {

    private int id;
    private int id_templo;
    private int id_profesor;
    private int id_alumno;
    private int id_tipo_clase;

    public Clase(int id, int id_templo, int profesor, int alumno, int id_tipo_clase) {
        this.id = id;
        this.id_templo = id_templo;
        this.id_profesor = profesor;
        this.id_alumno = alumno;
        this.id_tipo_clase = id_tipo_clase;
    }

    @Override
    public String toString() {
        return "Clase{" + "id=" + id + ", id_templo=" + id_templo + ", profesor=" + id_profesor + ", alumno=" + id_alumno + ", id_tipo_clase=" + id_tipo_clase + '}';
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_templo() {
        return id_templo;
    }

    public void setId_templo(int id_templo) {
        this.id_templo = id_templo;
    }

    public int getProfesor() {
        return id_profesor;
    }

    public void setProfesor(int profesor) {
        this.id_profesor = profesor;
    }

    public int getAlumno() {
        return id_alumno;
    }

    public void setAlumno(int alumno) {
        this.id_alumno = alumno;
    }

    public int getId_tipo_clase() {
        return id_tipo_clase;
    }

    public void setId_tipo_clase(int id_tipo_clase) {
        this.id_tipo_clase = id_tipo_clase;
    }
   
}
