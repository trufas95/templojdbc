/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author 34628
 */
public interface temploDOD {
    public List<Templo> selectAlltemplo();
    public Templo inserttemplo(int id, String nombre_templo, String ubicacion);
    public boolean deletetemplo(int id);
    public Templo updatetemplo(String nombre_templo, int id);
    //extra
    public ResultSet selecttemplo(String nombre_templo);
}
