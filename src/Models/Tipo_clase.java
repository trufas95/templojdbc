/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author 34628
 */
public class Tipo_clase {
    private int id;
private String nombre_clase;
private int precio;

    public Tipo_clase(int id, String nombre_clase, int precio) {
        this.id = id;
        this.nombre_clase = nombre_clase;
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "tipo_clase{" + "id=" + id + ", nombre_clase=" + nombre_clase + ", precio=" + precio + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre_clase() {
        return nombre_clase;
    }

    public void setNombre_clase(String nombre_clase) {
        this.nombre_clase = nombre_clase;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

}
