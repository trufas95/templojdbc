/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author 34628
 */
public interface ClaseDOD {
    
 public List<Clase>selectAllClase();
 public Clase insertClase (int id, int id_templo, int profesor, int alumno, int id_tipo_clase);
 public boolean deleteClase (int id);
 public Clase updateClase (int id_templo, int id);
 //extra
 public ResultSet selectClase (int id);
 
    
}
