/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.ResultSet;
import java.util.Date;
import java.util.List;


public interface alumnoDOD {
 
    public List<Alumno> selectAllalumno();
 public Alumno insertalumno (int id, String nombre_alumno, String rango, Date fecha_inicio);
 public boolean deletealumno (int id);
 public Alumno updatealumno (String nombre_alumno, int id);
 //extra
 public ResultSet selectalumno (String nombre_alumno);
    
}
