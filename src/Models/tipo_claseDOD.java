/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.ResultSet;
import java.util.List;

/**
 *
 * @author 34628
 */
public interface tipo_claseDOD {
    public List<Tipo_clase> selectAlltipo_clase();
    public Tipo_clase inserttipo_clase(int id, String nombre_clase, int precio);
    public boolean deletetipo_clase(int id);
    public Tipo_clase updatetipo_clase(String nombre_clase, int id);
    //extra
    public ResultSet selecttipo_clase(String nombre_clase);
}
