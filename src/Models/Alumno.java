/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;

/**
 *
 * @author 34628
 */
public class Alumno {

    private int id;
    private String nombre_alumno;
    private String rango;
    private Date fecha_inicio;

    public Alumno(int id, String nombre_alumno, String rango, Date fecha_inicio) {
        this.id = id;
        this.nombre_alumno = nombre_alumno;
        this.rango = rango;
        this.fecha_inicio = fecha_inicio;
    }

    @Override
    public String toString() {
        return "alumno{" + "id=" + id + ", nombre_alumno=" + nombre_alumno + ", rango=" + rango + ", fecha_inicio=" + fecha_inicio + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre_alumno() {
        return nombre_alumno;
    }

    public void setNombre_alumno(String nombre_alumno) {
        this.nombre_alumno = nombre_alumno;
    }

    public String getRango() {
        return rango;
    }

    public void setRango(String rango) {
        this.rango = rango;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

}
