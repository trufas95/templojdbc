/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author 34628
 */
public class Profesor {
    private int id;
private String nombre_profesor;
private String rango;

    public Profesor(int id, String nombre_profesor, String rango) {
        this.id = id;
        this.nombre_profesor = nombre_profesor;
        this.rango = rango;
    }

    @Override
    public String toString() {
        return "profesor{" + "id=" + id + ", nombre_profesor=" + nombre_profesor + ", rango=" + rango + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre_profesor() {
        return nombre_profesor;
    }

    public void setNombre_profesor(String nombre_profesor) {
        this.nombre_profesor = nombre_profesor;
    }

    public String getRango() {
        return rango;
    }

    public void setRango(String rango) {
        this.rango = rango;
    }

}
