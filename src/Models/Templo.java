/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author 34628
 */
public class Templo {
    private int id;
private String nombre_templo;
private String ubicacion;

    public Templo(int id, String nombre_templo, String ubicacion) {
        this.id = id;
        this.nombre_templo = nombre_templo;
        this.ubicacion = ubicacion;
    }

    @Override
    public String toString() {
        return "templo{" + "id=" + id + ", nombre_templo=" + nombre_templo + ", ubicacion=" + ubicacion + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre_templo() {
        return nombre_templo;
    }

    public void setNombre_templo(String nombre_templo) {
        this.nombre_templo = nombre_templo;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

}
