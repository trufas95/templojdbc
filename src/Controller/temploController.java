/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.Clase;
import Models.Templo;
import Models.temploDOD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 34628
 */
public class temploController implements temploDOD {
private final String SELECT_ALL = "select * from templo";
private final String INSERT_TEMPLO = "insert into templo(id, nombre_templo,ubicacion) values (?,?,?)";
    private Connection con;
    public temploController (Connection con) {
        this.con = con;
    }  

    @Override
    public List<Templo> selectAlltemplo() {
        PreparedStatement stm;
        ResultSet rs = null;
        List<Templo> lista = new ArrayList<Templo>();
        try {
            stm= this.con.prepareStatement(SELECT_ALL);
            rs= stm.executeQuery();
            
            while (rs.next()) {
                Templo t = new Templo(rs.getInt("id"), rs.getString("nombre_templo"), rs.getString("ubicacion"));
                lista.add(t);
            }
            
            stm.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        
        return lista;
    }

    @Override
    public Templo inserttemplo(int id, String nombre_templo, String ubicacion) {
      Templo templo = new Templo (id, nombre_templo, ubicacion);
        PreparedStatement ps;
        
           
       
        try {
            ps = con.prepareStatement(INSERT_TEMPLO);
        ps.setInt(1, id);
        ps.setString(2, nombre_templo);
        ps.setString(3, ubicacion);
        ps.executeUpdate();
        
        } catch( SQLException s){
            s.printStackTrace();
            
        }
        
        return templo;
    }

    @Override
    public boolean deletetemplo(int id) {
        String sql = "DELETE FROM templo WHERE id=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setInt(1, id);
            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("El templo " + id + " ha sido eliminado.");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public Templo updatetemplo(String nombre_templo, int id) {
        String sql = "UPDATE templo SET nombre_templo=? where id=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setString(1, nombre_templo);
            statement.setInt(2, id);

            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Se ha actualizado la clase");
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
       return null;
    }

    @Override
    public ResultSet selecttemplo(String nombre_templo) {
        return null;
    }
}
