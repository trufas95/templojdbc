/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.Clase;
import Models.Alumno;
import Models.alumnoDOD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class alumnoController implements alumnoDOD {

    private final String SELECT_ALL = "select * from alumno";
    private final String INSERT_ALUMNO = "insert into alumno(id, nombre_alumno,rango, fecha_inicio) values (?,?,?,?)";
    private Connection con;

    public alumnoController(Connection con) {
        this.con = con;
    }

    @Override
    public List<Alumno> selectAllalumno() {
        PreparedStatement stm;
        ResultSet rs = null;
        List<Alumno> lista = new ArrayList<Alumno>();
        try {
            stm = this.con.prepareStatement(SELECT_ALL);
            rs = stm.executeQuery();

            while (rs.next()) {
                Alumno a = new Alumno(rs.getInt("id"), rs.getString("nombre_alumno"), rs.getString("rango"), rs.getDate("fecha_inicio"));
                lista.add(a);
            }

            stm.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return lista;
    }

    @Override
    public Alumno insertalumno(int id, String nombre_alumno, String rango, Date fecha_inicio) {
        Alumno a = new Alumno(id, nombre_alumno, rango, fecha_inicio);
        PreparedStatement ps;
        try {

            ps = con.prepareStatement(INSERT_ALUMNO);
            ps.setInt(1, id);
            ps.setString(2, nombre_alumno);
            ps.setString(3, rango);
            ps.setDate(4, (java.sql.Date) fecha_inicio);
            ps.executeUpdate();

        } catch (SQLException s) {
            s.printStackTrace();

        }

        return a;
    }

    @Override
    public boolean deletealumno(int id) {
        
         String sql = "DELETE FROM alumno WHERE id=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setInt(1, id);
            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("El alumno " + id + " ha sido eliminada");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }

    @Override
    public Alumno updatealumno(String nombre_alumno, int id) {
        String sql = "UPDATE alumno SET nombre_alumno=? where id=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setString(1, nombre_alumno);
            statement.setInt(2, id);

            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Se ha actualizado el alumno.");
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public ResultSet selectalumno(String nombre_alumno) {
        return null;
    }
}
