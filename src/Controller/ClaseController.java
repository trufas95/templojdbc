/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.Clase;
import Models.ClaseDOD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClaseController implements ClaseDOD {

    private final String SELECT_ALL = "select * from clase";
    private final String INSERT_CLASE = "insert into clase(id, id_templo, id_profesor, id_alumno, id_tipo_clase) values (?,?,?,?,?)";
    private Connection con;

    public ClaseController(Connection con) {
        this.con = con;
    }

    @Override
    public List<Clase> selectAllClase() {
        PreparedStatement stm;
        ResultSet rs = null;
        List<Clase> lista = new ArrayList<Clase>();
        try {
            stm = this.con.prepareStatement(SELECT_ALL);
            rs = stm.executeQuery();

            while (rs.next()) {
                Clase c = new Clase(rs.getInt("id"), rs.getInt("id_templo"), rs.getInt("id_profesor"), rs.getInt("id_alumno"), rs.getInt("id_tipo_clase"));
                lista.add(c);
            }

            stm.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return lista;
    }

    @Override
    public Clase insertClase(int id, int id_templo, int id_profesor, int id_alumno, int id_tipo_clase) {
    Clase clase = new Clase (id, id_templo, id_profesor, id_alumno, id_tipo_clase);
        PreparedStatement ps;          
       
        try {
            
             ps = con.prepareStatement(INSERT_CLASE);
        ps.setInt(1, id);
        ps.setInt(2, id_templo);
        ps.setInt(3, id_profesor);
        ps.setInt(4, id_alumno);
        ps.setInt(5, id_tipo_clase);
        ps.executeUpdate();
        
        } catch( SQLException s){
            s.printStackTrace();
                   }
                     return clase;
    }

    @Override
    public boolean deleteClase(int id) {
       
        String sql = "DELETE FROM Clase WHERE id=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setInt(1, id);
            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("La clase " + id + " ha sido eliminada");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }
    
    @Override
    public Clase updateClase(int id_templo, int id) {
        String sql = "UPDATE Clase SET id_templo=? where id=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setInt(1, id_templo);
            statement.setInt(2, id);

            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Se ha actualizado la clase");
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public ResultSet selectClase(int id) {
        return null;
    }

}
