/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.Clase;
import Models.alumnoDOD;
import Models.Tipo_clase;
import Models.tipo_claseDOD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 34628
 */
public class tipo_claseController implements tipo_claseDOD {

    private final String SELECT_ALL = "select * from tipo_clase";
    private final String INSERT_TIPO_CLASE = "insert into tipo_clase (id, nombre_clase,precio) values (?,?,?)";
    private Connection con;

    public tipo_claseController(Connection con) {
        this.con = con;
    }

    @Override
    public List<Tipo_clase> selectAlltipo_clase() {
         PreparedStatement stm;
        ResultSet rs = null;
        List<Tipo_clase> lista = new ArrayList<Tipo_clase>();
        try {
            stm = this.con.prepareStatement(SELECT_ALL);
            rs = stm.executeQuery();
            
            while (rs.next()) {
                Tipo_clase c = new Tipo_clase(rs.getInt("id"), rs.getString("nombre_clase"), rs.getInt("precio"));
                lista.add(c);
            }
            
            stm.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return lista;
    }

    @Override
    public Tipo_clase inserttipo_clase(int id, String nombre_clase, int precio) {
       Tipo_clase tipo = new Tipo_clase (id, nombre_clase, precio);
        PreparedStatement ps; 
       
        try {
            ps = con.prepareStatement(INSERT_TIPO_CLASE);
        ps.setInt(1, id);
        ps.setString(2, nombre_clase);
        ps.setInt(3, precio);
        ps.executeUpdate();
        
        } catch( SQLException s){
            s.printStackTrace();
            
        }
        return tipo;
    }

    @Override
    public boolean deletetipo_clase(int id) {
        String sql = "DELETE FROM tipo_clase WHERE id=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setInt(1, id);
            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("El tipo_clase " + id + " ha sido eliminado.");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public Tipo_clase updatetipo_clase(String nombre_clase, int id) {
        String sql = "UPDATE tipo_clase SET nombre_clase=? where id=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setString(1, nombre_clase);
            statement.setInt(2, id);

            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Se ha actualizado la clase");
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public ResultSet selecttipo_clase(String nombre_clase) {
        return null;
    }

   
}
