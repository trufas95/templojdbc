/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.sql.Connection;
import Models.Clase;
import Models.Profesor;
import Models.profesorDOD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class profesorController implements profesorDOD {

    private final String SELECT_ALL = "select * from profesor";
    private final String INSERT_PROFESOR = "insert into profesor(id, nombre_profesor,rango) values (?,?,?)";
    private Connection con;

    public profesorController(Connection con) {
        this.con = con;
    }

    @Override
    public List<Profesor> selectAllprofesor() {
        PreparedStatement stm;
        List<Profesor> lista = new ArrayList<Profesor>();
        ResultSet rs = null;
        try {
            stm = this.con.prepareStatement(SELECT_ALL);
            rs = stm.executeQuery();

            while (rs.next()) {
                Profesor p = new Profesor(rs.getInt("id"), rs.getString("nombre_profesor"), rs.getString("rango"));
                lista.add(p);
            }

            stm.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return lista;
    }

    @Override
    public Profesor insertprofesor(int id, String nombre_profesor, String rango) {
        Profesor clase = new Profesor (id, nombre_profesor, rango);
        PreparedStatement ps;
        
           
       
        try {
             ps = con.prepareStatement(INSERT_PROFESOR);
        ps.setInt(1, id);
        ps.setString(2, nombre_profesor);
        ps.setString(3, rango);
        ps.executeUpdate();
        
        } catch( SQLException s){
            s.printStackTrace();
            
        }
        
        
        
        return clase;
    }

    @Override
    public boolean deleteprofesor(int id) {
        String sql = "DELETE FROM alumno WHERE id=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setInt(1, id);
            int rowsDeleted = statement.executeUpdate();
            if (rowsDeleted > 0) {
                System.out.println("El profesor " + id + " ha sido eliminado");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public Profesor updateprofesor(String nombre_profesor, int id) {
        String sql = "UPDATE profesor SET nombre_profesor=? where id=?";

        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(sql);
            statement.setString(1, nombre_profesor);
            statement.setInt(2, id);

            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Se ha actualizado el profesor.");
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public ResultSet selectprofesor(String nombre_profesor) {
        return null;
    }

}
